#!/bin/bash python3
import cv2
import numpy as np
import pickle, glob
import os

filelist = glob.glob('vali/*/*.jpg')
x = np.array([np.array(cv2.imread(fname)) for fname in filelist])
names = np.array([fname.split('/')[1] for fname in filelist])
print(names.shape)
count = 0
def union(a,b):
    x = min(a[0], b[0])
    y = min(a[1], b[1])
    w = max(a[0]+a[2], b[0]+b[2]) - x
    h = max(a[1]+a[3], b[1]+b[3]) - y
    return (x, y, w, h)

def intersection(a,b):
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0]+a[2], b[0]+b[2]) - x
    h = min(a[1]+a[3], b[1]+b[3]) - y
    if w<0 or h<0: return () # or (0,0,0,0) ?
    return (x, y, w, h)

for image in x: 
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(image_gray, 127,255,0)
    im, cont, hier = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    a,b,c,d = cv2.boundingRect(cont[0])
    for i in cont: 
        a_,b_,c_,d_ = cv2.boundingRect(i)
        a,b,c,d = union((a,b,c,d),(a_,b_,c_,d_))
    z = image[b:b+d, a:a+c]
    z = cv2.resize(z,(28,28),interpolation = cv2.INTER_AREA)
    try: 
        os.mkdir('test/' + str(names[count]))
    except OSError: 
        pass
        #print('f')
    cv2.imwrite('test/' + str(names[count]) + '/' + str(count) + "_" + str(len(cont))+ ".jpg", z)
    count+=1
