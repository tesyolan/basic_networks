import numpy
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils
from keras.models import load_model

from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D

from keras.preprocessing import image
from keras import backend as K
K.set_image_dim_ordering('th')
import pickle
import cv2
# encoding=utf8  

seed = 7
numpy.random.seed(seed)
class networks(): 
    def __init__(self): 
        self.num_pixels = 784
    def load_data(self): 
        with open('x_train.p','rb') as f: 
            self.x_train = pickle.load(f,encoding='utf-8')

        with open('y_train.p','rb') as f: 
            self.y_train = numpy.array(pickle.load(f,encoding='utf-8')).astype(numpy.float)

        with open('x_test.p','rb') as f:
            self.x_test = pickle.load(f,encoding='utf-8')

        with open('y_test.p','rb') as f: 
            self.y_test = numpy.array(pickle.load(f,encoding='utf-8')).astype(numpy.float)

    def multilayer_perceptron(self): 
        self.num_pixels = self.x_train.shape[1] * self.x_train.shape[2]
        self.x_train = self.x_train.reshape(self.x_train.shape[0],self.num_pixels).astype('float32')
        self.x_test = self.x_test.reshape(self.x_test.shape[0],self.num_pixels).astype('float32')
        self.x_train = self.x_train / 255
        self.x_test = self.x_test / 255
        self.y_train = np_utils.to_categorical(self.y_train)
        self.y_test = np_utils.to_categorical(self.y_test)
        num_classes = self.y_test.shape[1]
        print(self.num_pixels)
        print(num_classes)
        print(self.y_test.shape)
        self.model = Sequential()
        self.model.add(Dense(self.num_pixels,input_dim=self.num_pixels,kernel_initializer='normal',activation='relu'))
        self.model.add(Dense(num_classes,activation='softmax'))

        self.model.compile(loss='categorical_crossentropy',kernel_initializer='normal', optimizer='sgd', metrics=['accuracy'])

        self.model.fit(self.x_train,self.y_train,nb_epoch=40,batch_size=4)
        scores = self.model.evaluate(self.x_test,self.y_test,verbose=0)
        print("Error: %.2f%%" %(100-scores[1]*100))

    def simple_convolutional(self): 
        self.x_train = self.x_train.reshape(self.x_train.shape[0],1,28,28).astype('float32')
        self.x_test = self.x_test.reshape(self.x_test.shape[0],1,28,28).astype('float32')
        self.x_train = self.x_train / 255
        self.x_test = self.x_test / 255

        self.y_train = np_utils.to_categorical(self.y_train)
        self.y_test = np_utils.to_categorical(self.y_test)

        self.num_classes = self.y_test.shape[1]
        self.model = Sequential()
        self.model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))
        self.model.add(Flatten())
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dense(self.num_classes, activation='softmax'))
        # Compile model
        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

        self.model.fit(self.x_train, self.y_train, validation_data=(self.x_test,self.y_test), epochs=10, batch_size=4, verbose=2)
        scores = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        print("Error: %.2f%%" % (100-scores[1]*100))

    def save_model(self,name): 
        self.model.save(name + ".h5")
        self.model.save_weights(name + "weights.h5")
    
    def load_model(self, name):
        self.model = load_model(name + ".h5")
        self.model.load_weights(name + "weights.h5")
    def predict(self):
        img = 'test/00/0_1.jpg'
        im = cv2.imread(img) 
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = im /255
        #im = numpy.expand_dims(im, axis=1)
        print(im.shape)
        im = numpy.array(im.reshape(self.num_pixels).astype('float32'))

        predict = self.model.predict(numpy.array([im]))
        print(predict)


x = networks()
x.load_data()
#x.load_model()
#x.multilayer_perceptron()
x.simple_convolutional()
#x.save_model()
#x.predict()
