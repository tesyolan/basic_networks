var express = require('express'); 
var http = require('http'); 
var bodyParser = require('body-parser'); 
var cors = require('cors'); 

var imageRouter = require('./routes/routesImage'); 

var app = express(); 
app.use(bodyParser.json()); 
app.use(cors()); 
app.use(imageRouter); 
app.set('appName', 'reset_for_final_design'); 
app.set('port', process.env.PORT || 8000); 

http.createServer(app).listen(app.get('port'), function() {
console.log('Express Server is listening in port ' + app.get('port')); 
}); 
