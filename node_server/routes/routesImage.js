var express = require('express'); 
var bodyParser = require('body-parser'); 
var spawn = require('child_process').spawn; 

var imageRouter = express.Router(); 

imageRouter.route('/')
.get(function(req, res, next) {
	console.log('Go a get response at /'); 
})
.post(function(req, res, next){
	console.log('Getting POSt request at /'); 
	//console.log(req.body.image); 
	var process = spawn('python', ['/home/tyohannes/dataset/node_server/scripts/save_to_image.py', req.body.image])
	process.stdout.on('data',function (data) {
		console.log(data); 
		console.log('Returning call'); 
		res.writeHead(200, {'Content-Type': 'application/json'}); 
		res.end(JSON.stringify({'Result': 'Class'})); 
	}); 
}); 
module.exports = imageRouter; 
