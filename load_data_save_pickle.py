#!/bin/bash python3
import numpy as np
import pickle, glob
from PIL import Image

filelist = glob.glob('fidels_test/*/*.jpg')
x_test =  np.array([np.array(Image.open(name).convert('L').resize((28,28),Image.ANTIALIAS)) for name in filelist])
print("Loaded: %s images",x_test.shape)

with open('x_test.p','wb') as f:
    pickle.dump(x_test,f)

# Now let get the label for the test data. 
y = np.array([fname.split('/')[1] for fname in filelist])

print("Loaded: %s labels", y.shape)
with open('y_test.p','wb') as f:
    pickle.dump(y,f)

filelist = glob.glob('fidels/*/*.jpg')
x_train =  np.array([np.array(Image.open(name).convert('L').resize((28,28),Image.ANTIALIAS)) for name in filelist])
print("Loaded: %s images",x_train.shape)

with open('x_train.p', 'wb') as f:
    pickle.dump(x_train,f)

y = np.array([fname.split('/')[1] for fname in filelist])
print("Loaded: %s labels", y.shape)

with open('y_train.p','wb') as f:
    pickle.dump(y,f)
